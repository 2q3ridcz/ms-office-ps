﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path
$SrcFolderPath = $ProjectFolderPath | Join-Path -ChildPath "src\GooglePageIE"
$TestsFolderPath = $ProjectFolderPath | Join-Path -ChildPath "tests"

Write-Host("Here  : " + $here)
Write-Host("Source: " + $SrcFolderPath)
Write-Host("Test  : " + $TestsFolderPath)

$Date = Get-Date

Get-Module -ListAvailable Pester, PSScriptAnalyzer

$TestResultFolderPath = "$here\Output" | Resolve-Path
$TestResultFileName = $Date.ToSTring("yyyyMMdd-HHmmss") + "_" + "TestResult.xml"
$TestResultFilePath = $TestResultFolderPath | Join-Path -ChildPath $TestResultFileName

# $CodeCoverageFileName = $Date.ToSTring("yyyyMMdd-HHmmss") + "_" + "CodeCoverage.xml"
# $CodeCoverageFilePath = $TestResultFolderPath | Join-Path -ChildPath $CodeCoverageFileName

$SrcFileFullNames =
Get-ChildItem -Path $SrcFolderPath -Recurse |
?{ @(".ps1", ".psm1") -contains $_.Extension } |
%{ $_.FullName }

$BeforeProcess = @(Get-Process | ?{ @("EXCEL","et","wps","wpsupdate","ksomisc") -Contains $_.ProcessName } )
Write-Host ("OfficeProcess_Before (Count: " + $BeforeProcess.Length + ") :")
$BeforeProcess | %{ "" + $_.Id + " " + $_.ProcessName } | Write-Host

$TestResult = Invoke-Pester `
    -PassThru `
    -Script $TestsFolderPath `
    -OutputFile $TestResultFilePath `
    -OutputFormat NUnitXML `
    -CodeCoverage $SrcFileFullNames
    # -CodeCoverageOutputFile $CodeCoverageFilePath

$AfterProcess = @(Get-Process | ?{ @("EXCEL","et","wps","wpsupdate","ksomisc") -Contains $_.ProcessName } )
Write-Host ("OfficeProcess_After (Count: " + $AfterProcess.Length + ") :")
$AfterProcess | %{ "" + $_.Id + " " + $_.ProcessName } | Write-Host


# Comment out "Read-Host" when using this script in automated process.
Read-Host ("Press enter to exit...") | Out-Null

if($TestResult.FailedCount -gt 0) {
    ### Return to make GitLab CI job fail.
    EXIT 1
}
