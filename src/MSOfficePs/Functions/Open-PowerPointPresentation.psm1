﻿<#
.SYNOPSIS
    Opens a PowerPoint presentation file.
.DESCRIPTION
    Opens a PowerPoint presentation file with options for read-only, untitled, and window visibility.
.PARAMETER PowerPoint
    PowerPoint application object to use for opening the presentation.
.PARAMETER FileName
    Path to the PowerPoint presentation file to open.
.PARAMETER ReadOnly
    Opens the presentation in read-only mode.
.PARAMETER Untitled
    Opens the presentation without a title.
.PARAMETER WithWindow
    Controls whether the presentation opens with a visible window.
.EXAMPLE
    $PowerPoint = New-PowerPointApplication
    Open-PowerPointPresentation -PowerPoint $PowerPoint -FileName "presentation.pptx" -ReadOnly $true
#>
Function Open-PowerPointPresentation {
    [CmdletBinding()]
    [OutputType([Object])]
    param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNull()]
        [object]
        $PowerPoint
        ,
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [ValidateScript({Test-Path $_})]
        [string]
        $FileName
        ,
        [Parameter(Mandatory=$false)]
        [ValidateSet($True,$False)]
        [object]
        $ReadOnly = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [ValidateSet($True,$False)]
        [object]
        $Untitled = [Type]::Missing
        ,
        [Parameter(Mandatory=$false)]
        [ValidateSet($True,$False)]
        [object]
        $WithWindow = [Type]::Missing
    )
    begin {
        Write-Verbose "Starting PowerPoint presentation open operation"
    }
    process {
        $BeforeProcess = Get-Process

        try {
            Write-Verbose "Attempting to open presentation: $FileName"
            $presentation = $PowerPoint.Presentations.Open(
                $FileName,
                $ReadOnly,
                $Untitled,
                $WithWindow
            )

            Write-Verbose "Successfully opened presentation"

            $AfterProcess = Get-Process
            $newProcesses = $AfterProcess |
                Where-Object { $BeforeProcess.Id -NotContains $_.Id }

            $newProcesses | ForEach-Object {
                $processInfo = "" + $_.Id + " " + $_.ProcessName + " (Open-PowerPointPresentation)"
                Write-Verbose $processInfo
            }

            # Return the presentation object
            $presentation
        }
        catch {
            Write-Error "Failed to open presentation: $($_.Exception.Message)"
            throw
        }
    }
    end {
        Write-Verbose "Completed PowerPoint presentation open operation"
    }
}
