﻿Function Convert-ExcelFileToCsvFile {
    param (
        [Parameter(ValueFromPipeline=$True, Mandatory=$True)]
        [string]
        $BookFilePath
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $SheetName = ""
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $CsvFilePath
        ,
        [Parameter(Mandatory=$False)]
        [int[]]
        $DropRows = @()
        ,
        [Parameter(Mandatory=$False)]
        [int[]]
        $DropColumns = @()
        ,
        [Parameter(Mandatory=$False)]
        [switch]
        $Transpose = $False
    )
    begin {}
    process {
        ### read excel book and save as csv
        $Checkpoint = $Null
        try {
            $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False
            $Book = Open-ExcelBook -Excel $Excel -FileName $BookFilePath -ReadOnly $True
            $Sheet = $Null
            If ($PSBoundParameters.ContainsKey("SheetName")) {
                $Sheet = $Book.Worksheets.Item($SheetName)
            } Else {
                $Sheet = $Book.ActiveSheet
            }
            $DropRows | Sort-Object -Descending | %{ $Sheet.Rows.Item($_).Delete() } | Out-Null
            $DropColumns | Sort-Object -Descending | %{ $Sheet.Columns.Item($_).Delete() } | Out-Null

            $UsedRange = $Sheet.UsedRange
            If ($Transpose) {
                $Data = $Excel.WorksheetFunction.Transpose($UsedRange)
            } Else {
                $Data = $UsedRange.Value2
            }
            # TODO: Implement code for single row or single column.
            If ( $Data.Rank -Ne 2 ) { Throw ("TransposedDataRankError. Rank: " + $Data.Rank) }
            $RowLength = $Data.GetLength(0)
            $ColLength = $Data.GetLength(1)

            $CsvBook = $Excel.Workbooks.Add()
            $CsvSheet = $CsvBook.ActiveSheet
            $CsvSheet.Range($CsvSheet.Cells.Item(1,1), $CsvSheet.Cells.Item($RowLength, $ColLength)).Value2 = $Data
            $CsvBook | Save-ExcelAs -FileName $CsvFilePath -FileFormat 6 # 6: Csv
            $Checkpoint = "Success"
        } catch {
            Write-Error $_
        } finally {
            If ($CsvBook) {$CsvBook.Close($False); $CsvBook = $Null}
            If ($Book) {$Book.Close($False); $Book = $Null}
            If ($Excel) {$Excel.Quit(); $Excel = $Null}
            [GC]::Collect()
        }

        ### output book path
        If ($Checkpoint -Eq "Success") {
            $CsvFilePath | Write-Output
        }
    }
    end {}
}
