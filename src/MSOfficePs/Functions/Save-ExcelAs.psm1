﻿<#
.SYNOPSIS
    Saves an excel workbook.
.DESCRIPTION
    Saves an excel workbook.

    https://docs.microsoft.com/en-us/office/vba/api/excel.workbook.saveas
.EXAMPLE
    Save-ExcelAs -Workbook $Book -FileName $ExcelFilePath
#>
Function Save-ExcelAs {
    [CmdletBinding()]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSAvoidUsingPlainTextForPassword", "")]
    [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("PSUseSingularNouns", "")]
    [Alias()]
    [OutputType([void])]
    param (
        # Excel COM workbook object.
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   Position=0)]
        [object]
        $Workbook
        ,
        # A string that indicates the name of the file to be saved.
        [Parameter(Mandatory=$true,
                   Position=1)]
        [string]
        $FileName
        ,
        # The file format to use when you save the file.
        [Parameter(Mandatory=$false,
                   Position=2)]
        [object]
        $FileFormat = [Type]::Missing
        ,
        # A case-sensitive string (no more than 15 characters) that indicates the protection password to be given to the file.
        [Parameter(Mandatory=$false,
                   Position=3)]
        [object]
        $Password = [Type]::Missing
        ,
        # A string that indicates the write-reservation password for this file.
        [Parameter(Mandatory=$false,
                   Position=4)]
        [object]
        $WriteResPassword = [Type]::Missing
        ,
        # True to display a message when the file is opened, recommending that the file be opened as read-only.
        [Parameter(Mandatory=$false,
                   Position=5)]
        [bool]
        $ReadOnlyRecommended = $False
        ,
        # True to create a backup file.
        [Parameter(Mandatory=$false,
                   Position=6)]
        [bool]
        $CreateBackup = $False
        ,
        # The access mode for the workbook.
        [Parameter(Mandatory=$false,
                   Position=7)]
        [object]
        $AccessMode = [Type]::Missing
        ,
        # An XlSaveConflictResolution value that determines how the method resolves a conflict while saving the workbook.
        [Parameter(Mandatory=$false,
                   Position=8)]
        [object]
        $ConflictResolution = [Type]::Missing
        ,
        # True to add this workbook to the list of recently used files.
        [Parameter(Mandatory=$false,
                   Position=9)]
        [bool]
        $AddToMru = $False
        ,
        # Ignored for all languages in Microsoft Excel.
        [Parameter(Mandatory=$false,
                   Position=10)]
        [object]
        $TextCodepage = [Type]::Missing
        ,
        # Ignored for all languages in Microsoft Excel.
        [Parameter(Mandatory=$false,
                   Position=11)]
        [object]
        $TextVisualLayout = [Type]::Missing
        ,
        # True saves files against the language of Microsoft Excel (including control panel settings).
        [Parameter(Mandatory=$false,
                   Position=12)]
        [bool]
        $Local = $False
    )
    begin {}
    process {
        If ($PSBoundParameters.ContainsKey("FileFormat")) {
            $FileFormat = [Int]$FileFormat
        }

        $Workbook.SaveAs(
            $FileName,
            $FileFormat,
            $Password,
            $WriteResPassword,
            $ReadOnlyRecommended,
            $CreateBackup,
            $AccessMode,
            $ConflictResolution,
            $AddToMru,
            $TextCodepage,
            $TextVisualLayout,
            $Local
        )
    }
    end {}
}
