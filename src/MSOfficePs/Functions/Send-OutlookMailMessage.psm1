﻿Function Send-OutlookMailMessage {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [String[]]
        $To
        ,
        [Parameter(Mandatory=$true)]
        [string]
        $Subject
        ,
        [Parameter(Mandatory=$false)]
        [string]
        $Body = ""
        ,
        [Parameter(Mandatory=$false)]
        [string[]]
        $Bcc = @()
        ,
        [Parameter(Mandatory=$false)]
        [string[]]
        $Cc = @()
        ,
        [Parameter(Mandatory=$false)]
        [switch]
        $SaveOnly
    )

    begin {
        ### If outlook is up and running, gets it.
        ### If outlook is down, runs it.、But if outlook requires account, it returns error.
        $Outlook = New-Object -ComObject Outlook.Application
        If (-Not $Outlook) { Throw "Outlook is not running." }

        $RecipientType = @{
            To = [Microsoft.Office.Interop.Outlook.OlMailRecipientType]::OlTo
            Cc = [Microsoft.Office.Interop.Outlook.OlMailRecipientType]::OlCC
            Bcc = [Microsoft.Office.Interop.Outlook.OlMailRecipientType]::OlBCC
        }

    }

    process {
        $Mail = $Outlook.CreateItem([Microsoft.Office.Interop.Outlook.OlItemType]::OlMailItem)
        $Mail.BodyFormat = [Microsoft.Office.Interop.Outlook.OlBodyFormat]::OlFormatPlain

        foreach ($ParamName in @("Subject","Body")) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $Mail.($ParamName) = Get-Variable -Name $ParamName -ValueOnly
            }
        }

        foreach ($ParamName in @("To","Cc","Bcc")) {
            If ($PSBoundParameters.ContainsKey($ParamName)) {
                $TempVariable = Get-Variable -Name $ParamName -ValueOnly
                If ($TempVariable) {
                    $TempVariable |
                    %{
                        $Recipient = $Mail.Recipients.Add($_)
                        $Recipient.Type = $RecipientType[$ParamName]
                    }
                }
            }
        }

        If ($SaveOnly) {
            $Mail.Save()
        } Else {
            $Mail.Send()
        }
        $Mail = $Null
    }

    end {
        If ($Mail) { $Mail = $Null }
        If ($Outlook) { $Outlook = $Null }
        [GC]::Collect()
    }
}

