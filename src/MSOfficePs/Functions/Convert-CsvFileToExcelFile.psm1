﻿Function Convert-CsvFileToExcelFile {
    param (
        [Parameter(ValueFromPipeline=$True, Mandatory=$True)]
        [string]
        $CsvFilePath
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $BookFilePath
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $Encoding
        ,
        [Parameter(Mandatory=$False)]
        [string[]]
        $Columns
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $TemplateBookFilePath
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $TemplateSheetName
    )
    begin {}
    process {
        ### import csv
        $ImportCsvParam = @{"Path" = $CsvFilePath}
        If ($PSBoundParameters.ContainsKey("Encoding")) {
            $ImportCsvParam["Encoding"] = $Encoding
        }
        $CsvObjList = @(Import-Csv @ImportCsvParam)

        ### convert to object[,]
        If (-Not $PSBoundParameters.ContainsKey("Columns")) {
            $Columns = $CsvObjList | Get-Member -MemberType NoteProperty | %{ $_.Name }
        }

        $Csv2DArray = [Object[,]]::new($CsvObjList.Length + 1, $Columns.Length)
        foreach ($i in @(0..($Columns.Length-1))) {
            $Csv2DArray.SetValue($Columns[$i], 0, $i)
        }
        foreach ($j in @(0..($CsvObjList.Length - 1))) {
            $CsvObj = $CsvObjList[$j]
            foreach ($i in @(0..($Columns.Length - 1))) {
                $Csv2DArray.SetValue($CsvObj.($Columns[$i]), $j + 1, $i)
            }
        }

        ### create excel book
        $Checkpoint = $Null
        try {
            $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False

            $Book = $Null
            If ($PSBoundParameters.ContainsKey("TemplateBookFilePath")) {
                $Book = Open-ExcelBook -Excel $Excel -FileName $TemplateBookFilePath -ReadOnly $True
            } Else {
                $Book = $Excel.Workbooks.Add()
            }

            $Sheet = $Null
            If ($PSBoundParameters.ContainsKey("TemplateSheetName")) {
                $Sheet = $Book.Worksheets.Item($TemplateSheetName)
            } Else {
                $Sheet = $Book.ActiveSheet
            }

            $Range = $Sheet.Range($Sheet.Cells.Item(1,1), $Sheet.Cells.Item($Csv2DArray.GetLength(0), $Csv2DArray.GetLength(1)))
            $Range.Value = $Csv2DArray
            $Book | Save-ExcelAs -FileName $BookFilePath
            $Checkpoint = "Success"
        } catch {
            Write-Error $_
        } finally {
            If ($Book) {$Book.Close($False); $Book = $Null}
            If ($Excel) {$Excel.Quit(); $Excel = $Null}
            [GC]::Collect()
        }

        ### output book path
        If ($Checkpoint -Eq "Success") {
            $BookFilePath | Write-Output
        }
    }
    end {}
}
