﻿<#
.SYNOPSIS
    Adds a picture to excel worksheet.
.DESCRIPTION
    Adds a picture to excel worksheet.

    https://docs.microsoft.com/en-us/office/vba/api/excel.shapes.addpicture
.EXAMPLE
    $Shape = Add-ExcelShapePicture -Worksheet $Sheet -FileName $ImageFilePath
.OUTPUTS
    Excel COM shape object.
#>
Function Add-ExcelShapePicture {
    [CmdletBinding()]
    [Alias()]
    [OutputType([object])]
    param (
        # Excel COM worksheet object.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        [object]
        $Worksheet
        ,
        # The file from which the picture is to be created.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=1)]
        [string]
        $FileName
        ,
        # The file to link to.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=2)]
        [bool]
        $LinkToFile = $false
        ,
        # To save the picture with the document.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=3)]
        [bool]
        $SaveWithDocument = $true
        ,
        # The position (in points) of the upper-left corner of the picture relative to the upper-left corner of the document.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=4)]
        [int]
        $Left = 0
        ,
        # The position (in points) of the upper-left corner of the picture relative to the top of the document.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=5)]
        [int]
        $Top = 0
        ,
        # The width of the picture, in points (enter -1 to retain the width of the existing file).
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=6)]
        [int]
        $Width = -1
        ,
        # The height of the picture, in points (enter -1 to retain the height of the existing file).
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true,
                   Position=7)]
        [int]
        $Height = -1
    )
    begin {}
    process {
        $Worksheet.Shapes.AddPicture($FileName, $LinkToFile, $SaveWithDocument, $Left, $Top, $Width, $Height)
    }
    end {}
}
