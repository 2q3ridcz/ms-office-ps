﻿<#
.SYNOPSIS
    Adds a hyperlink to excel worksheet.
.DESCRIPTION
    Adds a hyperlink to excel worksheet.

    https://docs.microsoft.com/en-us/office/vba/api/excel.hyperlinks.add
.EXAMPLE
    $Hyperlink = Add-Hyperlink -Worksheet $Sheet -FileName $ImageFilePath
.OUTPUTS
    Excel COM hyperlink object.
#>
Function Add-Hyperlink {
    [CmdletBinding()]
    [Alias()]
    [OutputType([object])]
    param (
        # Excel COM worksheet object.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [object]
        $Worksheet
        ,
        # The anchor for the hyperlink. Can be either a Range or Shape object.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [object]
        $Anchor
        ,
        # The address of the hyperlink.
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true)]
        [AllowEmptyString()]
        [string]
        $Address
        ,
        # The subaddress of the hyperlink.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [object]
        $SubAddress = [Type]::Missing
        ,
        # The screen tip to be displayed when the mouse pointer is paused over the hyperlink.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [object]
        $ScreenTip = [Type]::Missing
        ,
        # The text to be displayed for the hyperlink.
        [Parameter(Mandatory=$false,
                   ValueFromPipelineByPropertyName=$true)]
        [object]
        $TextToDisplay = [Type]::Missing
    )
    begin {}
    process {
        $Worksheet.Hyperlinks.Add($Anchor, $Address, $SubAddress, $ScreenTip, $TextToDisplay)
    }
    end {}
}
