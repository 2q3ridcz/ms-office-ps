﻿Function Add-ExcelTOCSheet {
    param (
        [Parameter(Mandatory=$True)]
        [string]
        $InputPath
        ,
        [Parameter(Mandatory=$True)]
        [string]
        $OutputPath
        ,
        [Parameter(Mandatory=$False)]
        [string]
        $SheetName = "TOC"
    )
    begin {}
    process {
        $Checkpoint = $Null
        try {
            $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False
            $Book = $Excel.Workbooks.Add($InputPath)
            $SheetNameList = @($Book.Worksheets | %{$_.Name})

            If ($SheetNameList -contains $SheetName) {
                Write-Error ("Sheet '" + $SheetName + "' already exists!")
            } Else {
                $Sheet = $Book.Worksheets.Add($Book.Worksheets.Item(1))
                $Sheet.Name = $SheetName

                foreach ($i in @(0..($SheetNameList.Length - 1))) {
                    # $Sheet.Cells.Item($i+1,1) = $SheetNameList[$i]
                    $FuncParam = @{
                        "Worksheet" = $Sheet
                        "Anchor" = $Sheet.Cells.Item($i+1,1)
                        "Address" = ""
                        "SubAddress" = ("'" + $SheetNameList[$i] + "'!A1")
                        "ScreenTip" = $SheetNameList[$i]
                        "TextToDisplay" = $SheetNameList[$i]
                    }
                    Add-Hyperlink @FuncParam | Out-Null
                }
            }
            $Book | Save-ExcelAs -FileName $OutputPath
            $Checkpoint = "Success"
        } catch {
            Write-Error $_
        } finally {
            If ($Book) {$Book.Close($False); $Book = $Null}
            If ($Excel) {$Excel.Quit(); $Excel = $Null}
            [GC]::Collect()
        }

        ### output book path
        If ($Checkpoint -Eq "Success") {
            $OutputPath | Write-Output
        }
    }
    end {}
}
