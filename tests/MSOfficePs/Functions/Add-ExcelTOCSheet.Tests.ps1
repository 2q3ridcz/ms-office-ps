﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Add-ExcelTOCSheet" {
    Context "SheetName" {
        BeforeAll {
            function New-ExcelBookWithSheets ($Excel, $Path, $SheetNameList) {
                $Book = $Excel.Workbooks.Add()
                While ($SheetNameList.Length -Lt $Book.Worksheets.Count) {
                    $Book.Worksheets.Item($Book.Worksheets.Count).Delete()
                }
                While ($Book.Worksheets.Count -Lt $SheetNameList.Length) {
                    $Book.Worksheets.Add() | Out-Null
                }
                foreach ($i in @(0..($SheetNameList.Length - 1))) {
                    $Book.Worksheets.Item($i + 1).Name = $SheetNameList[$i]
                }
                $Book | Save-ExcelAs -FileName $Path
                If ($Book) {$Book.Close($False); $Book = $Null}
            }

            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose

                $BaseBookFilePath = "$TestDrive\" + "base-book.xlsx"
                $BaseBookSheetNameList = @("one-of-base","two-of-base")
                New-ExcelBookWithSheets -Excel $Excel -Path $BaseBookFilePath -SheetNameList $BaseBookSheetNameList
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
            }
        }

        $TestCases = @(
            @{
                "TestCase" = "SheetName not set";
                "SheetName" = $Null;
                "Expect" = @("TOC") + $BaseBookSheetNameList;
            }
            @{
                "TestCase" = "SheetName is set";
                "SheetName" = "目次";
                "Expect" = @("目次") + $BaseBookSheetNameList;
            }
        )
        It "Adds TOC sheet when <TestCase>" -TestCases $TestCases {
            param ($TestCase,$SheetName,$Expect)

            $Path = "$TestDrive\" + "Adds TOC sheet when $TestCase".Replace(" ","-") + ".xlsx"
            $Path | Should Not Exist

            $FuncParam = @{
                "InputPath" = $BaseBookFilePath
                "OutputPath" = $Path
            }
            If ($Null -Ne $SheetName) {$FuncParam["SheetName"] = $SheetName }
            Add-ExcelTOCSheet @FuncParam
            $Path | Should Exist

            $Checkpoints = @()
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = Open-ExcelBook -Excel $Excel -FileName $Path -Verbose
                $SheetNameList = @($Book.Worksheets | %{$_.Name})
                Assert-List -Result $SheetNameList -Expect $Expect

                $Sheet = $Book.Worksheets.Item($SheetNameList[0])
                $Sheet.Hyperlinks.Count | Should Be ($SheetNameList.Length - 1)

                $Checkpoints += "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
                $Checkpoints += "Success"
            }
            $Checkpoints.Length | Should Be 2
            $Checkpoints | Should Be "Success"
        }
    }
}
