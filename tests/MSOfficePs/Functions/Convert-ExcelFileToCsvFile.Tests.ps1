﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Convert-ExcelFileToCsvFile" {
    BeforeAll {
        $BookFilePath = "$TestDrive\input.xlsx"
        $SheetName = "tmpsht"
        try {
            $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False
            $Book = $Excel.Workbooks.Add()
            $Sheet = $Book.ActiveSheet
            $Sheet.Name = $SheetName

            $Data = [object[,]]::new(3,2)
            $Data[0,0] = "Date"
            $Data[1,0] = "2021/01/01"
            $Data[2,0] = "2021/01/02"
            $Data[0,1] = "Decimal"
            $Data[1,1] = 12.3456
            $Data[2,1] = 12345.6
            
            $Sheet.Range($Sheet.Cells.Item(1,1), $Sheet.Cells.Item(3,2)).Value = $Data
            $Book | Save-ExcelAs -FileName $BookFilePath
        } catch {
            Write-Error $_
        } finally {
            If ($Book) {$Book.Close($False); $Book = $Null}
            If ($Excel) {$Excel.Quit(); $Excel = $Null}
            [GC]::Collect()
        }
    }
    BeforeEach {
        $FuncParameter = @{
            "BookFilePath" = $BookFilePath
            "SheetName" = $SheetName
            "Verbose" = $True
        }
    }
    Context "Transpose" {
        $TestCases = @(
            @{"TestCase" = "non-transposed"; "Transpose" = $False}
            @{"TestCase" = "transposed"; "Transpose" = $True}
        )
        It "Creates <TestCase> csv file" -TestCases $TestCases {
            param ($TestCase, $Transpose)

            $CsvFilePath = "$TestDrive\" + "Creates $TestCase csv file".Replace(" ","-") + ".csv"
            $FuncParameter["CsvFilePath"] = $CsvFilePath
            $FuncParameter["Transpose"] = $Transpose
            Convert-ExcelFileToCsvFile @FuncParameter

            If ($Transpose) {
                $Expect = @(
                    "Date,44197,44198"
                    "Decimal,12.3456,12345.6"
                )
            } Else {
                $Expect = @(
                    "Date,Decimal"
                    "44197,12.3456"
                    "44198,12345.6"
                )
            }
            $Result = Get-Content -Path $CsvFilePath
            Assert-List -Result $Result -Expect $Expect
        }
    }
}
