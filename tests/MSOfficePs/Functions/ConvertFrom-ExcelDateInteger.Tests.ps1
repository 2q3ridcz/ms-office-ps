﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "ConvertFrom-ExcelDateInteger" {
    Context "DateInteger" {
        $TestCases = @(
            @{"DateInteger" = -1}
            @{"DateInteger" = 0}
            @{"DateInteger" = 60}
        )
        It "Throws when DateInteger is <DateInteger>" -TestCases $TestCases {
            param ($DateInteger)
            {ConvertFrom-ExcelDateInteger -DateInteger $DateInteger -ErrorAction Stop} | Should Throw
        }

        $TestCases = @(
            @{"DateInteger" = 1; "Expect" = [datetime]::new(1900,1,1)}
            @{"DateInteger" = 59; "Expect" = [datetime]::new(1900,2,28)}
            @{"DateInteger" = 61; "Expect" = [datetime]::new(1900,3,1)}
            @{"DateInteger" = 44562; "Expect" = [datetime]::new(2022,1,1)}
        )
        It "Returns <Expect> when DateInteger is <DateInteger>" -TestCases $TestCases {
            param ($DateInteger, $Expect)

            $Result = ConvertFrom-ExcelDateInteger -DateInteger $DateInteger
            $Result | Should Be $Expect
        }
    }
}
