﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Convert-CsvFileToExcelFile" {
    BeforeAll {
        $CsvFilePath = "$TestDrive\encoding-default.csv"
        @(
            , @("Date","Time","Datetime","String","Int","Decimal")
            , @("2021/01/01","06:51:00","2021/01/01 06:51:00","S12345","12345","12.3456")
            , @("2021/01/02","06:51:10","2021/01/02 06:51:10","ABCDEF","67890","12345.6")
        ) |
        %{ '"' + ($_ -join '","') + '"' } |
        Out-File -FilePath $CsvFilePath -Encoding Default

        $RowLength = 3
        $ColLength = 6
        $TemplateBookFilePath = "$TestDrive\template.xlsx"
        $TemplateSheetName = "tmpsht"
        try {
            $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False
            $Book = $Excel.Workbooks.Add()
            $Sheet = $Book.ActiveSheet
            $Sheet.Name = $TemplateSheetName

            $Sheet.Columns.Item(1).NumberFormatLocal = "yyyy/mm/dd" # "Date"
            $Sheet.Columns.Item(2).NumberFormatLocal = "hh:mm:dd" # "Datetime"
            $Sheet.Columns.Item(6).NumberFormatLocal = "hh:mm:dd" # "Time"
            $Book | Save-ExcelAs -FileName $TemplateBookFilePath
        } catch {
            Write-Error $_
        } finally {
            If ($Book) {$Book.Close($False); $Book = $Null}
            If ($Excel) {$Excel.Quit(); $Excel = $Null}
            [GC]::Collect()
        }
    }
    BeforeEach {
        $CCFTEFParameter = @{
            "CsvFilePath" = $CsvFilePath
            "Verbose" = $True
        }
    }
    Context "TemplateBookFilePath" {
        $TestCases = @(
            @{"TestCase" = "without using TemplateBook"; "UseTemplateBook" = $False}
            @{"TestCase" = "using TemplateBook"; "UseTemplateBook" = $True}
        )
        It "Creates excel file <TestCase>" -TestCases $TestCases {
            param ($TestCase, $UseTemplateBook)

            $BookFilePath = "$TestDrive\" + "Creates excel file $TestCase".Replace(" ","-") + ".xlsx"
            $CCFTEFParameter["BookFilePath"] = $BookFilePath
            If ($UseTemplateBook) { $CCFTEFParameter["TemplateBookFilePath"] = $TemplateBookFilePath }
            Convert-CsvFileToExcelFile @CCFTEFParameter

            $Checkpoints = @()
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = Open-ExcelBook -Excel $Excel -FileName $BookFilePath -Verbose
                $Sheet = $Book.ActiveSheet
                $Range = $Sheet.UsedRange
                $Data = $Range.Value()
                $Data.Rank | Should Be 2
                $Data.GetLength(0) | Should Be $RowLength
                $Data.GetLength(1) | Should Be $ColLength

                $Col = 1
                $Data.Get(1, $Col) | Should Be "Date"
                Assert-List -Result @(2..$Data.GetLength(0) | %{ $Data.Get($_, $Col) } | %{ Get-Date -Date $_ }) -Expect (@("2021/01/01","2021/01/02") | %{ Get-Date -Date $_ })
                $Col = 2
                $Data.Get(1, $Col) | Should Be "Datetime"
                Assert-List -Result @(2..$Data.GetLength(0) | %{ $Data.Get($_, $Col) } | %{ Get-Date -Date $_ }) -Expect (@("2021/01/01 06:51:00","2021/01/02 06:51:10") | %{ Get-Date -Date $_ })
                $Col = 3
                $Data.Get(1, $Col) | Should Be "Decimal"
                Assert-List -Result @(2..$Data.GetLength(0) | %{ $Data.Get($_, $Col) }) -Expect (@(12.3456,12345.6))
                $Col = 4
                $Data.Get(1, $Col) | Should Be "Int"
                Assert-List -Result @(2..$Data.GetLength(0) | %{ $Data.Get($_, $Col) }) -Expect (@(12345,67890))
                $Col = 5
                $Data.Get(1, $Col) | Should Be "String"
                Assert-List -Result @(2..$Data.GetLength(0) | %{ $Data.Get($_, $Col) }) -Expect (@("S12345","ABCDEF"))
                $Col = 6
                $Data.Get(1, $Col) | Should Be "Time"
                If ($UseTemplateBook) {
                    Assert-List -Result @(2..$Data.GetLength(0) | %{ $Data.Get($_, $Col) } | %{$_.ToString("HH:mm:ss")}) -Expect (@("06:51:00","06:51:10"))
                } Else {
                    Assert-List -Result @(2..$Data.GetLength(0) | %{ $Data.Get($_, $Col) } | %{$_.ToString("0.000000000000000")}) -Expect (@("06:51:00","06:51:10") | %{[Timespan]::new([datetime]::Parse(("0001/1/1 " + $_)).Ticks).TotalDays} | %{$_.ToString("0.000000000000000")})
                }

                $Checkpoints += "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
                $Checkpoints += "Success"
            }
            $Checkpoints.Length | Should Be 2
            $Checkpoints | Should Be "Success"
        }
    }
    Context "Columns" {
        $TestCases = @(
            @{"TestCase" = "with all columns in designated order"; "Columns" = @("Date","Time","Datetime","String","Int","Decimal")}
            @{"TestCase" = "with selected columns in designated order"; "Columns" = @("Datetime","Decimal")}
        )
        It "Creates excel file <TestCase>" -TestCases $TestCases {
            param ($TestCase, $Columns)

            $BookFilePath = "$TestDrive\" + "Creates excel file $TestCase".Replace(" ","-") + ".xlsx"
            $CCFTEFParameter["BookFilePath"] = $BookFilePath
            $CCFTEFParameter["Columns"] = $Columns
            Convert-CsvFileToExcelFile @CCFTEFParameter

            $Checkpoints = @()
            try {
                $Excel = New-ExcelApplication -Visible $False -ScreenUpdating $False -DisplayAlerts $False -Verbose
                $Book = Open-ExcelBook -Excel $Excel -FileName $BookFilePath -Verbose
                $Sheet = $Book.ActiveSheet
                $Range = $Sheet.UsedRange
                $Data = $Range.Value()
                $Data.Rank | Should Be 2

                Assert-List -Result @(1..$Data.GetLength(1) | %{ $Data.Get(1, $_) }) -Expect $Columns

                $Checkpoints += "Success"
            } finally {
                If ($Book) {$Book.Close($False); $Book = $Null}
                If ($Excel) {$Excel.Quit(); $Excel = $Null}
                [GC]::Collect()
                $Checkpoints += "Success"
            }
            $Checkpoints.Length | Should Be 2
            $Checkpoints | Should Be "Success"
        }
    }
}
