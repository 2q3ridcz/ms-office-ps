﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "Merge-PowerPointPresentation" {
    Context "Simple usecase" {
        BeforeAll {
            try {
                $PowerPoint = New-PowerPointApplication -Visible $False -Verbose

                $Target1Path = "$TestDrive\" + "Simple usecase-target-1".Replace(" ","-") + ".pptx"
                $Presentation = $PowerPoint.Presentations.Add()
                $Slide = $Presentation.Slides.Add(1, 1)
                $Slide.Shapes[1].TextFrame.TextRange.Text = "Slide1"
                $Presentation.Slides.Count | Should Be 1
                Save-PowerPointAs -Presentation $Presentation -FileName $Target1Path
                $Presentation.Close()
                $Presentation = $Null

                $Target2Path = "$TestDrive\" + "Simple usecase-target-2".Replace(" ","-") + ".pptx"
                $Presentation = $PowerPoint.Presentations.Add()
                $Slide = $Presentation.Slides.Add(1, 2)
                $Slide.Shapes[1].TextFrame.TextRange.Text = "Slide2"
                $Slide = $Presentation.Slides.Add(2, 3)
                $Slide.Shapes[1].TextFrame.TextRange.Text = "Slide3"
                $Presentation.Slides.Count | Should Be 2
                Save-PowerPointAs -Presentation $Presentation -FileName $Target2Path
                $Presentation.Close()
                $Presentation = $Null

                $Target3Path = "$TestDrive\" + "Simple usecase-target-3".Replace(" ","-") + ".pptx"
                $Presentation = $PowerPoint.Presentations.Add()
                $Slide = $Presentation.Slides.Add(1, 4)
                $Slide.Shapes[1].TextFrame.TextRange.Text = "Slide4"
                $Slide = $Presentation.Slides.Add(2, 5)
                $Slide.Shapes[1].TextFrame.TextRange.Text = "Slide5"
                $Slide = $Presentation.Slides.Add(3, 6)
                $Slide.Shapes[1].TextFrame.TextRange.Text = "Slide6"
                $Presentation.Slides.Count | Should Be 3
                Save-PowerPointAs -Presentation $Presentation -FileName $Target3Path
                $Presentation.Close()
                $Presentation = $Null
            } finally {
                If ($Presentation) {$Presentation.Close(); $Presentation = $Null}
                If ($PowerPoint) {$PowerPoint.Quit(); $PowerPoint = $Null}
                [GC]::Collect()
            }
        }

        It "Merges 3 files" {
            $Path = "$TestDrive\" + "Merges 3 files".Replace(" ","-") + ".pptx"

            $Result = "Failed"
            $MergeFilePathList = @($Target1Path, $Target2Path, $Target3Path)

            $Path | Should Not Exist
            Merge-PowerPointPresentation -Path $Path -MergeFilePathList $MergeFilePathList
            $Path | Should Exist

            try {
                $PowerPoint = New-PowerPointApplication -Visible $True -Verbose
                $Presentation = Open-PowerPointPresentation -PowerPoint $PowerPoint -FileName $Path -ReadOnly $True
                $Presentation.Slides.Count | Should Be 6
                $Presentation.Slides[1].Shapes[1].TextFrame.TextRange.Text | Should Be "Slide1"
                $Presentation.Slides[2].Shapes[1].TextFrame.TextRange.Text | Should Be "Slide2"
                $Presentation.Slides[3].Shapes[1].TextFrame.TextRange.Text | Should Be "Slide3"
                $Presentation.Slides[4].Shapes[1].TextFrame.TextRange.Text | Should Be "Slide4"
                $Presentation.Slides[5].Shapes[1].TextFrame.TextRange.Text | Should Be "Slide5"
                $Presentation.Slides[6].Shapes[1].TextFrame.TextRange.Text | Should Be "Slide6"

                $Result = "Success"
            } finally {
                If ($Presentation) {$Presentation.Close(); $Presentation = $Null}
                If ($PowerPoint) {$PowerPoint.Quit(); $PowerPoint = $Null}
                [GC]::Collect()
            }
            $Result | Should Be "Success"
        }
    }
}
